package com.mentatxx;

import android.content.*;
import android.app.Activity;

import com.mentatxx.RollerGameActivity;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.os.Handler;
import java.io.*;
import android.os.Bundle;

public class RollerGame extends CordovaPlugin {

  public static String ACTION_START_GAME = "startGame";
  public static String ACTION_STOP_GAME = "stopGame";
  public static String ACTION_SET_CALLBACK = "setCallback";

  CallbackContext _callbackContext;

  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    if (ACTION_START_GAME.equals(action)) {
      return startGame(callbackContext, args.getDouble(0), args.getString(1), args.getString(2), args.getString(3), args.getString(4));
    } else
    if (ACTION_STOP_GAME.equals(action)) {
      return stopGame(callbackContext);
    } else
    if (ACTION_SET_CALLBACK.equals(action)) {
      return setCallback(callbackContext);
    } else {
      callbackContext.error("RollerGame." + action + " is not a supported function");
      return false;
    }
  }

  public boolean startGame(CallbackContext callbackContext, double target, String title, String description, String boardUrl, String bottleUrl) {
    Intent intent = new Intent(cordova.getActivity(), RollerGameActivity.class);
    intent.putExtra("target", target);
    intent.putExtra("title", title);
    intent.putExtra("description", description);
    intent.putExtra("boardUrl", boardUrl);
    intent.putExtra("bottleUrl", bottleUrl);
    cordova.getActivity().startActivity(intent);
    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
    return true;
  }

  public boolean stopGame(CallbackContext callbackContext) {
    Activity rollerActivity = RollerGameActivity.fa;
    if (rollerActivity != null) {
      rollerActivity.finish();
    }
    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
    return true;
  }

  public boolean setCallback(CallbackContext callbackContext) {
    _callbackContext = callbackContext;
    PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
    pluginResult.setKeepCallback(true);
    callbackContext.sendPluginResult(pluginResult);
    RollerGameActivity.setListener(new RollerGameActivity.ResultListener() {
      public void onCompleted() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {
            // send callback
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
            pluginResult.setKeepCallback(true);
            _callbackContext.sendPluginResult(pluginResult);
            // close activity
            Activity rollerActivity = RollerGameActivity.fa;
            if (rollerActivity != null) {
              rollerActivity.finish();
            }
          }
        }, 3500);

      }
    });
    return true;
  }

}
