package com.mentatxx;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.app.Activity;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RollerGameActivity extends Activity /*AppCompatActivity*/ {

    private static final String TAG = "RollerGameActivity";

    public interface ResultListener
    {
        public void onCompleted();
    }

    public static ArrayList<ResultListener> listeners = new ArrayList<ResultListener> ();
    public static void setListener(ResultListener listener)
    {
	    listeners.clear();
	    listeners.add(listener);
    }

    public class ImageLoader extends AsyncTask<Void, Void, Void> {
        Bitmap b;
        ImageView _img;
        String _url;
        String _fallback;
        Activity _activity;
        Runnable _nextRunnable;

        ImageLoader(String url, ImageView img, String fallback, Activity activity, Runnable nextRunnable) {
            _url = url;
            _img = img;
            _fallback = fallback;
            _activity = activity;
            _nextRunnable = nextRunnable;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                try {
                  URL url = new URL(_url);
                  InputStream is = new BufferedInputStream(url.openStream());
                  b = BitmapFactory.decodeStream(is);
                } catch (Exception ignored) {
                  int drawableResourceId = _activity.getResources().getIdentifier(_fallback, "drawable", _activity.getPackageName());
                  b = BitmapFactory.decodeResource(_activity.getResources(), drawableResourceId);
                }
                images.put(_fallback, b);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
          _img.setImageBitmap(b);
          if (_nextRunnable != null) {
            new Thread(_nextRunnable).start();
          }
        }
    }

    ImageView board;
    ImageView bottle;
    TextView titleTextView;
    TextView spinFasterTextView;

    private int NUMBER_ASYNCTASK = 2;
    private int counter = 0;

    double slowing = 0.2; // initialSpeed - slowing*t = 0
    double maxTime = 10.0;
    boolean rotated = false;
    // The ‘active pointer’ is the one currently moving our object.
    private int INVALID_POINTER_ID = -1;
    private int mActivePointerId = INVALID_POINTER_ID;

    private float mLastTouchX, mLastTouchY;
    private float mPosX, mPosY;

    public float currentAngle = 0.0f;
    public long currentTime = 0;
    public float startAngle = 0.0f;
    public long startTime = 0;
    public float targetAngle = 2.0f;
    public float maxSpeed = 400.0f;
    public float minAngle = (float) (30.0f*Math.PI/180.0f);

    public static Activity fa = null;
    public HashMap<String, Bitmap> images = new HashMap<String, Bitmap>();

  private Runnable lastRunnable = new Runnable() {
      @Override
      public void run() {
       runOnUiThread(new Runnable() { public void run(){
         counter++;
         if (counter == NUMBER_ASYNCTASK) {
           // all loading tasks are completed
           Bitmap boardBitmap = images.get("board");
           Bitmap bottleBitmap = images.get("bottle");
           int w = Math.min(board.getMeasuredHeight(), Math.min(board.getMeasuredWidth(), boardBitmap.getWidth()));
           int h0 = bottleBitmap.getHeight();
           int h = h0*w/bottleBitmap.getWidth();
           Bitmap bMapScaled = Bitmap.createScaledBitmap(
             bottleBitmap,
             w,
             h,
             true);
           bottle.setImageBitmap(bMapScaled);
         }
        }});
      }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
          currentAngle = savedInstanceState.getFloat("currentAngle");
          startAngle = savedInstanceState.getFloat("startAngle");
          rotated = savedInstanceState.getBoolean("rotated");
        }
        setContentView(this.getResources().getIdentifier("activity_roller_game", "layout", this.getPackageName()));
	      fa = this;
        //
        board = (ImageView) findViewById(this.getResources().getIdentifier("imageView", "id", this.getPackageName()));
        bottle = (ImageView) findViewById(this.getResources().getIdentifier("imageView2", "id", this.getPackageName()));
	      titleTextView = (TextView) findViewById(this.getResources().getIdentifier("titleTextView", "id", this.getPackageName()));
        spinFasterTextView = (TextView) findViewById(this.getResources().getIdentifier("spinFasterTextView", "id", this.getPackageName()));
        targetAngle = (float) getIntent().getExtras().getDouble("target");
        String boardUrl = getIntent().getExtras().getString("boardUrl");
        String bottleUrl = getIntent().getExtras().getString("bottleUrl");
        String title = getIntent().getExtras().getString("title");
        String description = getIntent().getExtras().getString("description");
        new ImageLoader(boardUrl, board, "board", this, lastRunnable).execute();
        new ImageLoader(bottleUrl, bottle, "bottle", this, lastRunnable).execute();
	      titleTextView.setText(title);
        bottle.setRotation((float)(currentAngle*180/Math.PI));
        Log.d(TAG, "onCreate "+currentAngle);
        findViewById(this.getResources().getIdentifier("closeButton", "id", this.getPackageName())).setOnClickListener(handleCancelClick);
        findViewById(this.getResources().getIdentifier("spinButton", "id", this.getPackageName())).setOnClickListener(handleSpinClick);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      // Save the user's current game state
      savedInstanceState.putFloat("currentAngle", currentAngle);
      savedInstanceState.putFloat("startAngle", startAngle);
      savedInstanceState.putBoolean("rotated", rotated);

      // Always call the superclass so it can save the view hierarchy state
      super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
	    super.onDestroy();
	    fa = null;
    }

    public void spinRoller(double fromPosition, double toPosition, double speed) {
        if (rotated) return;
        if (Math.abs(speed)>maxSpeed) {
          if (speed>0) {
            speed = maxSpeed;
          } else {
            speed = -maxSpeed;
          }
        }
        double t = Math.abs(speed / slowing);
        if (t > maxTime) {
            t = maxTime;
        }
        double realSlowing = speed / t;

        double distance = fromPosition + speed * t - realSlowing * t * t / 2.0;
        double target, loops;
        loops = Math.floor(distance / (2.0 * Math.PI));
        if (loops < 1.0d && loops > -1.0d) {
            loops = loops>0 ? 1.0d : -1.0d;
        }
        target = toPosition + 2.0 * Math.PI * loops;

        RotateAnimation animation = new RotateAnimation(
                (float)(fromPosition*180/Math.PI),
                (float)(target*180/Math.PI),
                bottle.getWidth()/2,
                bottle.getHeight()/2
        );
        animation.setDuration((long)(t*1000.0));
        animation.setFillAfter(true);
        animation.setInterpolator(new DecelerateInterpolator());
	      animation.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
              currentAngle = targetAngle;
              for (ResultListener listener : listeners) {
                listener.onCompleted();
              }
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });
        bottle.startAnimation(animation);

        Log.v(TAG, "from: "+fromPosition+" to: "+target+ " speed: "+speed);
        rotated = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        final int action = MotionEventCompat.getActionMasked(ev);

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                if (rotated) break;
                final int pointerIndex = MotionEventCompat.getActionIndex(ev);
                final float x = MotionEventCompat.getX(ev, pointerIndex);
                final float y = MotionEventCompat.getY(ev, pointerIndex);

                // Remember where we started (for dragging)
                mLastTouchX = x;
                mLastTouchY = y;
                mPosX = 0;
                mPosY = 0;
                View view = findViewById(android.R.id.content);
                startAngle = (float) Math.atan2(mLastTouchY-(board.getHeight()/2.0), mLastTouchX-(board.getWidth()/2.0));
                startTime = ev.getEventTime();
                Log.d(TAG, "Start angle "+startAngle);

                // Save the ID of this pointer (for dragging)
                mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
                return true;
            }

            case MotionEvent.ACTION_MOVE: {
                if (rotated) break;
                // Find the index of the active pointer and fetch its position
                final int pointerIndex =
                        MotionEventCompat.findPointerIndex(ev, mActivePointerId);

                final float x = MotionEventCompat.getX(ev, pointerIndex);
                final float y = MotionEventCompat.getY(ev, pointerIndex);

                // Calculate the distance moved
                final float dx = x - mLastTouchX;
                final float dy = y - mLastTouchY;

                mPosX += dx;
                mPosY += dy;

                // Remember this touch position for the next move event
                mLastTouchX = x;
                mLastTouchY = y;
                View view = findViewById(android.R.id.content);
                currentAngle = (float) Math.atan2(mLastTouchY-(board.getHeight()/2.0), mLastTouchX-(board.getWidth()/2.0));
                currentTime = ev.getEventTime();
                bottle.setRotation((float) (currentAngle*180.0/Math.PI));

                Log.v(TAG, String.format("Rotate: %f", currentAngle));

                return true;
            }

            case MotionEvent.ACTION_UP: {
                Log.v(TAG, "Action UP");
                if (rotated) break;
                // Find the index of the active pointer and fetch its position
                final int pointerIndex = MotionEventCompat.findPointerIndex(ev, mActivePointerId);

                final float x = MotionEventCompat.getX(ev, pointerIndex);
                final float y = MotionEventCompat.getY(ev, pointerIndex);

                // Calculate the distance moved
                final float dx = x - mLastTouchX;
                final float dy = y - mLastTouchY;

                mPosX += dx;
                mPosY += dy;

                View view = findViewById(android.R.id.content);
                mLastTouchX = x;
                mLastTouchY = y;
                currentAngle = (float) Math.atan2(mLastTouchY-(board.getHeight()/2.0), mLastTouchX-(board.getWidth()/2.0));
                currentTime = ev.getEventTime();
                float speed = 600.0f*(currentAngle-startAngle)/(currentTime-startTime);
                Log.d(TAG, "Speed "+speed);
                if (Math.abs(currentAngle-startAngle)<minAngle) {
                  blinkSpinFaster();
                  return true;
                }
                bottle.setRotation(0);
                spinRoller(currentAngle, targetAngle, speed);
                mActivePointerId = INVALID_POINTER_ID;
                return true;
            }

            case MotionEvent.ACTION_CANCEL: {
                Log.v(TAG, "Action CANCEL");
                if (rotated) break;
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {
                Log.v(TAG, "Action POINTERUP");

                final int pointerIndex = MotionEventCompat.getActionIndex(ev);
                final int pointerId = MotionEventCompat.getPointerId(ev, pointerIndex);

                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = MotionEventCompat.getX(ev, newPointerIndex);
                    mLastTouchY = MotionEventCompat.getY(ev, newPointerIndex);
                    mActivePointerId = MotionEventCompat.getPointerId(ev, newPointerIndex);
                }
                break;
            }
        }
        return false;
    }

//  private void blinkSpinFaster() {
//    spinFasterTextView.setVisibility(View.VISIBLE);
//    new Thread(new Runnable() {
//      @Override
//      public void run() {
//        int timeToBlink = 2000;    //in milliseconds
//        try { Thread.sleep(timeToBlink); } catch (Exception ignored) {}
//        runOnUiThread(new Runnable() { public void run(){
//          spinFasterTextView.setVisibility(View.INVISIBLE);
//        }});
//      }
//    }).start();
//  }

    private void blinkSpinFaster() {
        final Activity that = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(that)
                            .setTitle("Too slow")
                            .setMessage("Spin faster!")
                            .setCancelable(false)
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Whatever...
                                }
                            }).create().show();
                }
            }
        });
    }

  private View.OnClickListener handleCancelClick = new View.OnClickListener() {
    public void onClick(View arg0) {
      fa.finish();
    }
  };

  private View.OnClickListener handleSpinClick = new View.OnClickListener() {
    public void onClick(View arg0) {
      spinRoller(0.0, targetAngle, Math.random()*10.0+1.0);
    }
  };

}
