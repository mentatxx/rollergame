#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#import <Cordova/CDVInvokedUrlCommand.h>
#import "ViewController.h"

@interface RollerGame : CDVPlugin <GameRollerDelegate>

- (void) startGame:(CDVInvokedUrlCommand*) command;
- (void) stopGame:(CDVInvokedUrlCommand*) command;
- (void) setCallback:(CDVInvokedUrlCommand*) command;

- (void) gameOver;
- (void) closeView;
@end