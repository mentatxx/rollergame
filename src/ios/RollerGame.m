#include "RollerGame.h"
#include "ViewController.h"

@implementation RollerGame

NSString* successcallbackId;
CDVPluginResult* successCallback;

BOOL opened = NO;
ViewController* vc;

- (void) startGame:(CDVInvokedUrlCommand*) command {
    if (opened) return;
    opened = YES;
    vc = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    vc.targetAngle = [command.arguments[0] doubleValue];
    vc.titleText = command.arguments[1];
    vc.descriptionText = command.arguments[2];
    vc.boardUrl = command.arguments[3];
    vc.bottleUrl = command.arguments[4];
    vc.resultDelegate = self;
    [self.viewController presentViewController:vc animated:NO completion:^{
//        opened = NO;
//        dispatch_after(0, dispatch_get_main_queue(), ^{
//            [self.viewController dismissViewControllerAnimated:NO completion:nil];
//        });
    }];
}

- (void) stopGame:(CDVInvokedUrlCommand*) command {
    if (!opened) return;
    opened = NO;
    dispatch_after(0, dispatch_get_main_queue(), ^{
        [self.viewController dismissViewControllerAnimated:NO completion:nil];
    });
}

- (void) closeView {
    if (!opened) return;
    opened = NO;
    dispatch_after(0, dispatch_get_main_queue(), ^{
        [self.viewController dismissViewControllerAnimated:NO completion:nil];
    });

}

- (void) setCallback:(CDVInvokedUrlCommand*) command {
    successcallbackId = [command callbackId];
    successCallback = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES];
    [successCallback setKeepCallback:[NSNumber numberWithBool:YES]];
}

- (void) gameOver {
    successCallback = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [successCallback setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:successCallback callbackId:successcallbackId];
}

@end