//
//  TouchableView.h
//  rollergame
//
//  Created by Alexey Petushkov on 23.04.16.
//  Copyright © 2016 Alexey Petushkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TouchableViewDelegate <NSObject>

- (void) movedFrom: (CGPoint) startPoint atStartTime: (NSTimeInterval) startTime toPoint: (CGPoint) toPoint toTime: (NSTimeInterval) toTime;
- (void) moveCompletedFrom: (CGPoint) startPoint atStartTime: (NSTimeInterval) startTime toPoint: (CGPoint) toPoint toTime: (NSTimeInterval) toTime;
@end

@interface TouchableView : UIView
@property (weak) IBOutlet id <TouchableViewDelegate> touchableDelegate;

@end
