//
//  TouchableView.m
//  rollergame
//
//  Created by Alexey Petushkov on 23.04.16.
//  Copyright © 2016 Alexey Petushkov. All rights reserved.
//

#import "TouchableView.h"

@implementation TouchableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

NSTimeInterval startTime;
CGPoint startPoint;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (![touches count]) return;
    startTime = event.timestamp;
    UITouch* touch = [[touches allObjects] objectAtIndex:0];
    startPoint = [touch locationInView:self];

}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (![touches count]) return;
    NSTimeInterval timestamp = event.timestamp;
    UITouch* touch = [[touches allObjects] objectAtIndex:0];
    CGPoint currentPoint = [touch locationInView:self];
    if (!_touchableDelegate) return;
    [_touchableDelegate movedFrom:startPoint atStartTime:startTime toPoint:currentPoint toTime:timestamp];
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (![touches count]) return;
    NSTimeInterval timestamp = event.timestamp;
    UITouch* touch = [[touches allObjects] objectAtIndex:0];
    CGPoint currentPoint = [touch locationInView:self];
    if (!_touchableDelegate) return;
    [_touchableDelegate moveCompletedFrom:startPoint atStartTime:startTime toPoint:currentPoint toTime:timestamp];
}

@end
