//
//  ViewController.h
//  rollergame
//
//  Created by Alexey Petushkov on 20.04.16.
//  Copyright © 2016 Alexey Petushkov. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol GameRollerDelegate <NSObject>
- (void) gameOver;
- (void) closeView;
@end

@interface ViewController<TouchableViewDelegate> : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardBoard;
@property (weak, nonatomic) IBOutlet UIImageView *bottle;
@property (weak, nonatomic) IBOutlet id<GameRollerDelegate> resultDelegate;
@property (assign) CGFloat targetAngle;
@property (strong, nonatomic) NSString* titleText;
@property (strong, nonatomic) NSString* descriptionText;
@property (strong, nonatomic) NSString* boardUrl;
@property (strong, nonatomic) NSString* bottleUrl;

- (IBAction)closeClick:(id)sender;
- (IBAction)spinClick:(id)sender;

@end

