//
//  ViewController.m
//  rollergame
//
//  Created by Alexey Petushkov on 20.04.16.
//  Copyright © 2016 Alexey Petushkov. All rights reserved.
//

#import <math.h>
#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *spinFasterLabel;
- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag;
@end

@implementation ViewController

// User started to rotate
BOOL rotated = NO;
// Rotation completed
BOOL rotationCompleted = NO;
// User closed that
BOOL closed = NO;

- (UIImage*) getImagePath: (NSString*) image ofType: (NSString*) type{
    NSString* imagePath = [[NSBundle mainBundle] pathForResource:image ofType:type];
    return [UIImage imageWithContentsOfFile: imagePath];
}

- (UIImage*) loadImageFromUrl: (NSString*) imageUrl {
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imageUrl]];
    return [UIImage imageWithData: imageData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    _cardBoard.image = [self loadImageFromUrl:self.boardUrl];
    if (!_cardBoard.image) {
        _cardBoard.image = [self getImagePath:@"board" ofType:@"png"];
    }
    _cardBoard.contentMode = UIViewContentModeRedraw;
    _bottle.image = [self loadImageFromUrl:self.bottleUrl];
    if (!_bottle.image) {
        _bottle.image = [self getImagePath:@"bottle" ofType:@"png"];
    }
    _bottle.contentMode = UIViewContentModeRedraw;

    self.titleLabel.text = self.titleText;
    self.descriptionLabel.text = self.descriptionText;
}

CGFloat slowing = 0.2; // initialSpeed - slowing*t = 0
CGFloat maxTime = 10.0;
CGFloat maxSpeed = 5.0;
CGFloat minAngle = 30.0 * M_PI / 180.0;

- (void) startRotation: (CGFloat) fromPosition to: (CGFloat) toPosition withInitialSpeed: (CGFloat) speed {
    if (rotated) return;
    double realSpeed = speed;
    if (fabs(realSpeed)>maxSpeed) {
        if (realSpeed>0) {
            realSpeed = maxSpeed;
        } else {
            realSpeed = -maxSpeed;
        }
    }
    double t = fabs(realSpeed / slowing);
    if (t > maxTime) {
        t = maxTime;
    }
    double realSlowing = realSpeed / t;
    
    double distance = fromPosition + realSpeed * t - realSlowing * t * t / 2.0;
    double target, loops;
    loops = floor(distance/(2.0 * M_PI));
    if (loops<1 &&  loops>-1) { loops=loops>0?1.0:-1.0;}
    target = toPosition + 2.0 * M_PI * loops;
    realSlowing = (target - fromPosition)/(t * t / 2.0);
    realSpeed = realSlowing*t;
    //NSLog(@"Loops %.2f Distance: %.2f Target %.2f ToPosition: %.2f", loops, distance, target, toPosition);
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"transform.rotation.z";
    NSMutableArray* values = [[NSMutableArray alloc] init];
    NSMutableArray* keys = [[NSMutableArray alloc] init];
    for (CGFloat tt = 0.0; tt<t; tt=tt+0.3) {
        CGFloat v = fromPosition + realSpeed * tt - realSlowing * tt * tt / 2.0;
        [values addObject:@(v)];
        [keys addObject:@(tt/t)];
        // NSLog(@"%.2f %.2f", v, tt/t );
    }
    [values addObject:@(target)];
    [keys addObject:@(1.0)];
    // NSLog(@"%.2f %.2f", target, 1.0 );
    NSLog(@"Slow %.5f Time: %.2f Target: %.2f Speed: %.2f FromPos: %.2f", realSlowing, t, target, realSpeed, fromPosition);
    animation.values = [values copy];
    animation.keyTimes = [keys copy];
    animation.duration = t;
    animation.calculationMode = kCAAnimationLinear;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.delegate = self;
    [_bottle.layer addAnimation:animation forKey:@"rotation"];
    rotated = YES;
}

- (void)viewDidLayoutSubviews {
    CGRect rect;
    //align cardboard size
    rect = _cardBoard.frame;
    CGRect viewFrame = [self view].bounds;
    NSLog(@"View frame %.0f %.0f", viewFrame.size.width, viewFrame.size.height);
    rect.size.width = MIN(rect.size.width, 2.0*viewFrame.size.width/3.0);
    rect.size.height = rect.size.width;
    rect.origin.x = (viewFrame.size.width - rect.size.width) / 2;
    _cardBoard.frame = rect;
    CGRect cardRect = rect;
    //
    rect = _bottle.frame;
    CGSize imageSize = _bottle.image.size;
    NSLog(@"Image %.0f %.0f", imageSize.width, imageSize.height);
    rect.size.width = cardRect.size.width * (4.0/5.0);
    rect.size.height = rect.size.width * imageSize.height / imageSize.width;
    rect.origin.x = (viewFrame.size.width - rect.size.width) / 2;
    rect.origin.y = _cardBoard.frame.origin.y + (_cardBoard.frame.size.height - rect.size.height) / 2;
    
    _bottle.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    _bottle.contentMode = UIViewContentModeScaleAspectFit;
    NSLog(@"Rect %.0f %.0f %.0f %.0f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag {
    rotationCompleted = YES;
    double delayInSeconds = 3.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (!closed) {
            [self.resultDelegate gameOver];
        }
        rotationCompleted = NO;
        rotated = NO;
        closed = NO;
        [self.resultDelegate closeView];
    });

}

- (CGFloat) getAngle: (CGPoint) point {
    CGRect rect = _bottle.frame;
    CGFloat dx = point.x - (rect.origin.x + rect.size.width / 2);
    CGFloat dy = point.y - (rect.origin.y + rect.size.height / 2);
    return atan2(dy, dx);
}

CGFloat currentAngle = 0;

- (void) movedFrom: (CGPoint) startPoint atStartTime: (NSTimeInterval) startTime toPoint: (CGPoint) toPoint toTime: (NSTimeInterval) toTime {
    if (rotated) return;
    currentAngle = [self getAngle: toPoint] - [self getAngle: startPoint];
    _bottle.layer.transform = CATransform3DMakeRotation(currentAngle, 0, 0.0, 1.0);
}

- (void) moveCompletedFrom: (CGPoint) startPoint atStartTime: (NSTimeInterval) startTime toPoint: (CGPoint) toPoint toTime: (NSTimeInterval) toTime {
    if (rotated) return;
    currentAngle = [self getAngle: toPoint] - [self getAngle: startPoint];
    if (currentAngle<minAngle) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Too slow"
                                                        message:@"Spin faster!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    [self startRotation:currentAngle to:_targetAngle withInitialSpeed:currentAngle/(toTime-startTime)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeClick:(id)sender {
    rotated = NO;
    if (!rotationCompleted) {
        closed = YES;
    }
    [self.resultDelegate closeView];
}

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

- (IBAction)spinClick:(id)sender {
    CGFloat initialSpeed = [self randomFloatBetween:3.0 and:6.0];
    [self startRotation:currentAngle to:_targetAngle withInitialSpeed:initialSpeed];
}
@end
