// WARNING! IF YOU SEE ANY LINE ABOVE THIS LINE IT COULD INDICATE MIGRATION ERROR

var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');

var PLUGIN_NAME = 'RollerGame';

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function RollerGame() {

}

/**
 * Options is an object:
 * {
 *   table: [{name: "Win", start:0, end: 20}, ...],
 *   target: "Win",
 *   boardUrl: 'http://foo.bar/board.png',
 *   bottleUrl: 'http://foo.bar/bottle.png',
 *   title: '',
 *   description: ''
 * }
 *
 * @param options
 * @param resultCallback
 * @param cb
 * @param cbError
 */
RollerGame.prototype.startGame = function (options, resultCallback, cb, cbError) {
    var that = this,
        target;

    function callback() {
        that.resultCallback(target)
    }

    if (!options.table || !(options.table instanceof Array)) {
        cbError(new Error('Missing table'));
        return;
    }
    if (!options.target) {
        cbError(new Error('Missing target'));
        return;
    }
    if (typeof resultCallback !== 'function') {
        cbError(new Error('Missing result callback'));
        return;
    }
    if (!options.boardUrl || !options.boardUrl || !options.bottleUrl || !options.title || !options.description) {
        cbError(new Error('Missing some options'));
        return;
    }
    this.resultCallback = resultCallback;
    target = options.target;
    var possibleSectors = [];
    for (var i = 0; i < options.table.length; i++) {
        if (options.table[i].name === target) possibleSectors.push(options.table[i]);
    }
    if (!possibleSectors.length) {
        cbError(new Error('Cannot win'));
        return;
    }
    var sectorIndex = getRandomInt(0, possibleSectors.length),
        sector = possibleSectors[sectorIndex];
    var angleRange = sector.end - sector.start,
        angle = sector.start + angleRange * 0.1 + 0.8 * angleRange * Math.random(),
        angleRadian = Math.PI * angle / 180.0;
    exec(callback, callback, PLUGIN_NAME, 'setCallback', []);
    exec(cb, cbError, PLUGIN_NAME, 'startGame', [
        angleRadian,
        options.title,
        options.description,
        options.boardUrl,
        options.bottleUrl
    ]);
};

RollerGame.prototype.stopGame = function (cb) {
    exec(cb, cb, PLUGIN_NAME, 'stopGame', []);
};

module.exports = new RollerGame();